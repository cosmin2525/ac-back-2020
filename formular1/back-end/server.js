const mysql = require("mysql");
const express = require("express");
const bodyParser = require("body-parser");

//Initializing server
const app = express();
app.use(bodyParser.json());
const port = 8081;
app.listen(port, () => {
  console.log("Server online on: " + port);
});
app.use("/", express.static("../front-end"));
const connection = mysql.createConnection({
  host: "localhost",
  user: "root",
  password: "",
  database: "formular07"
});
connection.connect(function(err) {
  console.log("Connected to database!");
  const sql =
    "CREATE TABLE IF NOT EXISTS jucatori_inscrisi(nume VARCHAR(255),prenume VARCHAR(255),telefon VARCHAR(255),email VARCHAR(255),facultate VARCHAR(255),Echipă VARCHAR(255))";
  connection.query(sql, function(err, result) {
    if (err) throw err;
  });
});
app.post("/inscriere", (req, res) => {
    const student ={
        nume: req.body.nume,
        prenume: req.body.prenume,
        telefon:req.body.telefon,
        email:req.body.email,
        facultate: req.body.facultate,
        Echipă: req.body.Echipă,
    }
  let error = [];

  if (!student.nume||!student.prenume||!student.telefon||!student.email||!student.facultate||!student.Echipă) {
    console.log("Unul sau mai multe campuri nu au fost introduse!");
    error.push("Unul sau mai multe campuri nu au fost introduse");
  } else {
    if (student.nume.length < 2 || student.nume.length > 30) {
      console.log("Nume invalid!");
      error.push("Nume invalid");
    } else if (!student.nume.match("^[A-Za-z]+$")) {   
      console.log("Numele trebuie sa contina doar litere!");
      error.push("Numele trebuie sa contina doar litere!");
    }
    if (student.prenume.length < 2 || student.nume.length > 30) {
      console.log("Prenume invalid!");
      error.push("Prenume invalid!");
    } else if (!student.prenume.match("^[A-Za-z]+$")) {
      console.log("Prenumele trebuie sa contina doar litere!");
      error.push("Prenumele trebuie sa contina doar litere!");
    }
    if (student.telefon.length != 10) {
      console.log("Numarul de telefon trebuie sa fie de 10 cifre!");
      error.push("Numarul de telefon trebuie sa fie de 10 cifre!");
    } else if (!student.telefon.match("^[0-9]+$")) {
      console.log("Numarul de telefon trebuie sa contina doar cifre!");
      error.push("Numarul de telefon trebuie sa contina doar cifre!");
    }
    if (!student.email.includes("@gmail.com") && !student.email.includes("@yahoo.com") && !student.email.includes("@yahoo.ro") && !student.email.includes("@gmail.ro")) {
      console.log("Email invalid!");
      error.push("Email invalid!");
    }
    if(!student.facultate.match("^[A-Za-z]+$")) {
        console.log("Denumirea facultatii trebuie sa contina doar litere!");
        error.push("Denumirea facultatii trebuie sa contina doar litere!");
    }
    if (student.Echipă.length < 2 || student.Echipă.length > 30) {
      console.log("Echipă invalidă!");
      error.push("Echipă invalidă!");
  }
  if (error.length === 0) {
    const sql =
      `INSERT INTO jucatori_inscrisi (nume,prenume,telefon,email,facultate,Echipă) VALUES (?,?,?,?,?,?)`;
    connection.query(sql,
        [
        student.nume, 
        student.prenume, 
        student.telefon, 
        student.email, 
        student.facultate,
        student.Echipă,
        ],
        function(err, result) {
            if (err) throw err;
            console.log("Jucător inscris in baza de date!");
            res.status(200).send({
                message: "Jucător inscris in baza de date!"
            });
        console.log(sql);
    });
  } else {
    res.status(500).send(error);
    console.log("Eroare la inserarea in baza de date!");
  }

}});