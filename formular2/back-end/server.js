const mysql = require("mysql");
const express = require("express");
const bodyParser = require("body-parser");

const app = express();
app.use(bodyParser.json());
const port = 5050;
app.listen(port, () => {
  console.log("Server online on: " + port);
});
app.use("/", express.static("../front-end"));
const connection = mysql.createConnection({
  host: "localhost",
  user: "root",
  password: "",
  database: "siscit_back_end",
});
connection.connect(function (err) {
  console.log("Connected to database!");
  const sql =
    "CREATE TABLE IF NOT EXISTS abonamente_metrou(nume VARCHAR(255), prenume  VARCHAR(255), email  VARCHAR(255) , telefon  VARCHAR(255),data_inceput  VARCHAR(10),data_sfarsit  VARCHAR(10))"; //intre paranteze campurile cu datele
  connection.query(sql, function (err, result) {
    if (err) throw err;
  });
});
app.post("/bilet", (req, res) => {
  let bilet = {
    nume: req.body.nume,
    prenume: req.body.prenume,
    telefon: req.body.telefon,
    cnp: req.body.cnp,
    email: req.body.email,
    data_inceput: req.body.data_inceput,
    data_sfarsit: req.body.data_sfarsit,
    varsta: req.body.varsta,
  };
  let error = [];
  console.log(bilet);

  //aici rezolvati cerintele (づ｡◕‿‿◕｡)づ
 if ( !bilet.nume||!bilet.prenume||!bilet.telefon||!bilet.email||!bilet.data_inceput||!bilet.data_sfarsit )
 {
   error.push("Unul sau mai multe campuri nu au fost introduse");
   console.log("Unul sau mai multe campuri nu au fost introduse");
 } else {
   if ( bilet.nume.length < 2 || bilet.nume.length>30 ){
     console.log("Nume invalid");
     error.push("Nume invalid");
   } else if (!bilet.nume.match("^[A-Za-z]+$" )){
     console.log("Numele trebuie sa contina doar litere!");
     error.push("Numele trebuie sa contina doar litere" );
   }
   if ( bilet.prenume.length < 2 || bilet.prenume.length > 30 ){
     console.log("Prenume invalid!");
     error.push("Prenume invalid!");
   } else if ( !bilet.prenume.match("^[A-Za-z]+$")) {
     console.log("Prenumele trebuie sa contina doar litere!");
     error.push("Prenumele trebuie sa contina doar litere!");
   }
   if ( bilet.telefon.length != 10 ) {
     console.log("Numarul de telefon trebuie sa fie de 10 cifre!");
     error.push("Numarul de telefon trebuie sa fie de 10 cifre!");
   } else if ( !bilet.telefon.match("^[0-9]+$")) {
     console.log("Numarul de telefon trebuie sa contina doar cifre!");
     error.push("Numarul de telefon trebuie sa contina doar cifre!");
   }
   if (!bilet.email.includes("@gmail.com") && !bilet.email.includes("@yahoo.com") && !bilet.email.includes("@yahoo.ro") && !bilet.email.includes("@gmail.ro")) {
    console.log("Email invalid!");
    error.push("Email invalid!");
   }
   var dateformat = /^(0?[1-9]|[12][0-9]|3[01])[\/\-](0?[1-9]|1[012])[\/\-]\d{4}$/;
   if (!bilet.data_inceput.match(dateformat) || !bilet.data_sfarsit.match(dateformat) ) {
     console.log("Data introdusa gresit!");
     error.push("Data introdusa gresit!");
   }
   var tmp_d = bilet.data_inceput.split('/')
   var start_d = new Date(tmp_d[2] + "-" + tmp_d[1] + "-" +tmp_d[0]);
   var tmp_d = bilet.data_sfarsit.split('/')
   var end_d = new Date(tmp_d[2] + "-" + tmp_d[1] + "-" +tmp_d[0]);
 
   if ( bilet.data_inceput > bilet.data_sfarsit ){
    console.log("Data invalida inceput mai mare ca sfarsit");
    error.push("Data invalida inceput mai mare ca sfarsit");
   }
   var today = new Date();
   if ( bilet.data_inceput > today || bilet.data_sfarsit < today){
    console.log("Data invalida inceput mai mare ca azi");
    error.push("Data invalida inceput mai mare ca azi");
   }
 
   if ( today < bilet.data_inceput || today > bilet.data_sfarsit )
   {
     console.log("Abonament expirat!");
     error.push("Abonament expirat!");
   }
   if ( !bilet.cnp ) {
     console.log("Campul cnp este gol!");
     error.push("Campul cnp este gol!");
   }
   if ( bilet.cnp.toString().length != 13 ) {
     console.log("Cnp invalid!");
     error.push("Cnp invalid!");
   } else if ( !bilet.cnp.match("^[0-9]+$") ) {
     console.log("Cnp contine doar cifre!");
     error.push("Cnp contine doar cifre!");
   }
   if ( !bilet.varsta ) {
     console.log("Campul varsta este gol!");
     error.push("Campul varsta este gol!");
   }
   if ( !bilet.varsta.match("^[0-9]+$") ) {
     console.log("Varsta contine doar cifre!");
     error.push("Varsta contine doar cifre!");
   }

   if ( bilet.varsta < 0 || bilet.varsta >= 1000 ) {
     console.log("Varsta gresita!");
     error.push("Varsta gresita!");
   }
   var x = new Date();
   var an = x.getFullYear();
  
   var sex_an_cnp = Math.floor(bilet.cnp/10000000000)
   var sex_cnp = Math.floor(sex_an_cnp /100)
   var an_cnp_fin = sex_an_cnp%100
   var milenium = 1900
   if (sex_cnp >=5){
     milenium = 2000
   }
   an_cnp_fin += milenium
   console.log(an_cnp_fin)
   if ( an%10000-an_cnp_fin != bilet.varsta ) {
     console.log("Varsta gresita!");
     error.push("Varsta gresita!");
   }
   

var gen = 'M'
if (sex_cnp == 6 || sex_cnp == 4|| sex_cnp == 2){
gen = 'F'
}




  if (error.length === 0) {

    const sql = `INSERT INTO abonamente_metrou (nume,
      prenume,
      telefon,
      cnp,
      email,
      data_inceput,
      data_sfarsit,
      varsta,
      gen) VALUES (?,?,?,?,?,?,?,?,?)`;
    connection.query(
      sql,
      [
        bilet.nume,
        bilet.prenume,
        bilet.telefon,
        bilet.cnp,
        bilet.email,
        bilet.data_inceput,
        bilet.data_sfarsit,
        bilet.varsta,
        gen,
      ],
      function (err, result) {
        if (err) throw err;
        console.log("Abonament realizat cu succes!");
        res.status(200).send({
          message: "Abonament realizat cu succes",
        });
        console.log(sql);
      }
    );
  } else {
    res.status(500).send(error);
    console.log("Abonamentul nu a putut fi creat!");
  }
  app.use('/', express.static('../front-end'))
}});
//modifica si din front la index.html
